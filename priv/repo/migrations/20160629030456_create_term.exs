defmodule Ioquiz.Repo.Migrations.CreateTerm do
  use Ecto.Migration

  def change do
    create table(:terms) do
      add :term, :string, null: false
      add :definition, :text, null: false
      add :lesson_id, references(:lessons, on_delete: :nothing)

      timestamps()
    end
    create index(:terms, [:lesson_id])

  end
end
