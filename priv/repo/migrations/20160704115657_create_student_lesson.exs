defmodule Ioquiz.Repo.Migrations.CreateStudentLesson do
  use Ecto.Migration

  def change do
    create table(:student_lessons) do
      add :user_id, references(:users, on_delete: :nothing)
      add :lesson_id, references(:lessons, on_delete: :nothing)

      timestamps()
    end
    create index(:student_lessons, [:user_id])
    create index(:student_lessons, [:lesson_id])

  end
end
