defmodule Ioquiz.Repo.Migrations.ChangeTermTypeToText do
  use Ecto.Migration

  def change do
    alter table(:terms) do
      modify :term, :text
    end
  end
end
