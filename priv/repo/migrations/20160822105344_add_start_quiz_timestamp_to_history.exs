defmodule Ioquiz.Repo.Migrations.AddStartQuizTimestampToHistory do
  use Ecto.Migration

  def change do
    alter table(:history) do
      add :quiz_started_at, :datetime
    end
  end
end
