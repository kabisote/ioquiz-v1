defmodule Ioquiz.Repo.Migrations.CreateHistory do
  use Ecto.Migration

  def change do
    create table(:history) do
      add :user_id, references(:users, on_delete: :nothing)
      add :lesson_id, references(:lessons, on_delete: :nothing)
      add :score, :float, default: 0.0

      timestamps()
    end
    create index(:history, [:user_id])
    create index(:history, [:lesson_id])
  end
end
