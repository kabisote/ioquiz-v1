defmodule Ioquiz.Repo.Migrations.CreateMistakesTable do
  use Ecto.Migration

  def change do
    create table(:mistakes) do
      add :history_id, references(:history, on_delete: :nothing)
      add :term_id, references(:terms, on_delete: :nothing)

      timestamps()
    end
    create index(:mistakes, [:history_id])
    create index(:mistakes, [:term_id])
  end
end
