defmodule Ioquiz.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :name, :string
      add :username, :string
      add :role, :string
      add :password_hash, :string
      add :details, :text

      timestamps()
    end
    create unique_index(:users, [:username])

  end
end
