defmodule Ioquiz.Repo.Migrations.CreateLesson do
  use Ecto.Migration

  def change do
    create table(:lessons) do
      add :title, :string, null: false
      add :subject_id, references(:subjects, on_delete: :nothing)

      timestamps()
    end
    create index(:lessons, [:subject_id])

  end
end
