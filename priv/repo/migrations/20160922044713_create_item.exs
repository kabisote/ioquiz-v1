defmodule Ioquiz.Repo.Migrations.CreateItem do
  use Ecto.Migration

  def change do
    create table(:items) do
      add :content, :text
      add :type, :integer, default: 1
      add :status, :integer, default: 1
      add :question_id, references(:questions, on_delete: :delete_all)

      timestamps()
    end
    create index(:items, [:question_id])

  end
end
