defmodule Ioquiz.Repo.Migrations.CreateSubject do
  use Ecto.Migration

  def change do
    create table(:subjects) do
      add :title, :string, null: false

      timestamps()
    end
    create unique_index(:subjects, [:title])

  end
end
