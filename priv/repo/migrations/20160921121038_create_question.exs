defmodule Ioquiz.Repo.Migrations.CreateQuestion do
  use Ecto.Migration

  def change do
    create table(:questions) do
      add :question, :text
      add :type, :integer, default: 1
      add :status, :integer, default: 1
      add :lesson_id, references(:lessons, on_delete: :delete_all)

      timestamps()
    end
    create index(:questions, [:lesson_id])

  end
end
