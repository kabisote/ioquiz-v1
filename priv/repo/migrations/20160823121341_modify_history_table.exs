defmodule Ioquiz.Repo.Migrations.ModifyHistoryTable do
  use Ecto.Migration

  def change do
    alter table(:history) do
      modify :score, :integer, default: 0
      add :total_items, :integer, default: 0
    end
  end
end
