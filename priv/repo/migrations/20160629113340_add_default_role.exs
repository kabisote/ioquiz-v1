defmodule Ioquiz.Repo.Migrations.AddDefaultRole do
  use Ecto.Migration

  def change do
    alter table(:users) do
      remove :role
      add :role_level, :integer, default: 3
    end
  end
end
