# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Ioquiz.Repo.insert!(%Ioquiz.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Ioquiz.Repo
alias Ioquiz.User

user_params = %{"name" => "Administrator", "username" => "admin", "role_level" => "1", "password" => "ay-ukis"}
changeset = User.registration_changeset(%User{}, user_params)
Repo.insert!(changeset)
