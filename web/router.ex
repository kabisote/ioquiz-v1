defmodule Ioquiz.Router do
  use Ioquiz.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Ioquiz.Auth, repo: Ioquiz.Repo
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Ioquiz do
    pipe_through :browser # Use the default browser stack

    get  "/", PageController, :index

    get  "/students/:username", StudentController, :show
    get  "/students/:username/history", StudentController, :history
    get  "/students/:username/lesson/:lesson_id/review", StudentController, :review
    get  "/students/:username/lesson/:lesson_id/quiz", StudentController, :quiz
    post  "/students/:username/lesson/:lesson_id/quiz", StudentController, :submit_quiz
    post  "/students", StudentController, :assign_lesson
    delete  "/students/:student_id/lesson/:lesson_id", StudentController, :unassign_lesson

    resources "/sessions", SessionController, only: [:new, :create, :delete]
  end

  scope "/manage", Ioquiz do
    pipe_through [:browser, :authenticate_user, :check_role_level] # Use the default browser stack

    resources "/users", UserController
    resources "/subjects", SubjectController, except: [:show] do
      resources "/lessons", LessonController, except: [:show] do
        resources "/questions", QuestionController, except: [:show] do
          resources "/items", ItemController, except: [:show]
        end
        resources "/terms", TermController, except: [:show]
      end
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", Ioquiz do
  #   pipe_through :api
  # end
end
