defmodule Ioquiz.SessionController do
  use Ioquiz.Web, :controller

  def new(conn, _) do
    conn
    |> put_layout("session.html")
    |> render("new.html")
  end

  def create(conn, %{"session" => %{"username" => user, "password" => pass}}) do
    case Ioquiz.Auth.login_by_username_and_pass(conn, user, pass, repo: Repo) do
      {:ok, conn} ->
        conn
        |> put_flash(:info, "Welcome back!")
        |> redirect(to: user_path(conn, :index))
      {:error, _reason, conn} ->
        conn
        |> put_layout("session.html")
        |> put_flash(:error, "Invalid username/password combination")
        |> render("new.html")
    end
  end

  def delete(conn, _) do
    conn
    |> Ioquiz.Auth.logout()
    |> redirect(to: page_path(conn, :index))
  end
end
