defmodule Ioquiz.UserController do
  use Ioquiz.Web, :controller

  alias Ioquiz.Lesson
  alias Ioquiz.StudentLesson
  alias Ioquiz.User

  def index(conn, _params) do
    query = from u in User, order_by: u.name

    # Using this format because of this warning: the variable "query" is unsafe as it has been set inside a case/cond/receive/if/&&/||
    query =
      if conn.assigns.current_user.role_level > 1 do
        query
        |> where([u], u.role_level == 3)
      else
        query
      end

    users = Repo.all(query)
    render(conn, "index.html", users: users)
  end

  def new(conn, _params) do
    changeset = User.changeset(%User{})
    render(conn, "new.html", changeset: changeset, show_password_field: true)
  end

  def create(conn, %{"user" => user_params}) do
    changeset = User.registration_changeset(%User{}, user_params)

    case Repo.insert(changeset) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "#{user.name} created successfully.")
        |> redirect(to: user_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    user =
      User
      |> Repo.get!(id)
      |> Repo.preload(:lessons) 

    # Todo: Allow filtering of lessons by subject.
    lessons = 
      Lesson
      |> Repo.all
      |> Enum.map(fn(lesson) -> {Map.fetch!(lesson, :title), Map.fetch!(lesson, :id)} end)

    student_lesson_changeset = StudentLesson.changeset(%StudentLesson{})

    render(conn, "show.html", user: user, lessons: lessons, student_lesson_changeset: student_lesson_changeset)
  end

  def edit(conn, %{"id" => id}) do
    user = Repo.get!(User, id)
    changeset = User.changeset(user)
    render(conn, "edit.html", user: user, changeset: changeset, show_password_field: false)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Repo.get!(User, id)
    changeset = User.changeset(user, user_params)

    case Repo.update(changeset) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "User updated successfully.")
        |> redirect(to: user_path(conn, :show, user))
      {:error, changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Repo.get!(User, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(user)

    conn
    |> put_flash(:info, "User deleted successfully.")
    |> redirect(to: user_path(conn, :index))
  end
end
