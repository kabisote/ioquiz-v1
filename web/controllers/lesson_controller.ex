defmodule Ioquiz.LessonController do
  use Ioquiz.Web, :controller

  alias Ioquiz.Subject
  alias Ioquiz.Lesson

  def index(conn, %{"subject_id" => subject_id}) do
    subject = Repo.get!(Subject, subject_id)
    lessons = Repo.all(subject_lessons(subject))

    render(conn, "index.html", subject: subject, lessons: lessons)
  end

  def new(conn, %{"subject_id" => subject_id}) do
    subject = Repo.get!(Subject, subject_id)
    changeset = Lesson.changeset(%Lesson{})

    render(conn, "new.html", subject: subject, changeset: changeset)
  end

  def create(conn, %{"subject_id" => subject_id, "lesson" => lesson_params}) do
    subject = Repo.get!(Subject, subject_id)

    changeset = 
      subject
      |> build_assoc(:lessons)
      |> Lesson.changeset(lesson_params)

    case Repo.insert(changeset) do
      {:ok, _lesson} ->
        conn
        |> put_flash(:info, "Lesson created successfully.")
        |> redirect(to: subject_lesson_path(conn, :index, subject))
      {:error, changeset} ->
        render(conn, "new.html", subject: subject, changeset: changeset)
    end
  end

  def edit(conn, %{"subject_id" => subject_id, "id" => id}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(subject_lessons(subject), id)

    changeset = Lesson.changeset(lesson)

    render(conn, "edit.html", subject: subject, lesson: lesson, changeset: changeset)
  end

  def update(conn, %{"subject_id" => subject_id, "id" => id, "lesson" => lesson_params}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(subject_lessons(subject), id)

    changeset = Lesson.changeset(lesson, lesson_params)

    case Repo.update(changeset) do
      {:ok, _lesson} ->
        conn
        |> put_flash(:info, "Lesson updated successfully.")
        |> redirect(to: subject_lesson_path(conn, :index, subject))
      {:error, changeset} ->
        render(conn, "edit.html", subject: subject, lesson: lesson, changeset: changeset)
    end
  end

  def delete(conn, %{"subject_id" => subject_id, "id" => id}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(subject_lessons(subject), id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(lesson)

    conn
    |> put_flash(:info, "Lesson deleted successfully.")
    |> redirect(to: subject_lesson_path(conn, :index, subject))
  end

  defp subject_lessons(subject) do
    assoc(subject, :lessons)
  end
end
