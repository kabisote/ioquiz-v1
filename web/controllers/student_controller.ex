defmodule Ioquiz.StudentController do
  use Ioquiz.Web, :controller

  plug :authenticate_user when action in [:show, :history]

  alias Ioquiz.History
  alias Ioquiz.Lesson
  alias Ioquiz.Mistake
  alias Ioquiz.StudentLesson
  alias Ioquiz.Term
  alias Ioquiz.User

  def show(conn, %{"username" => username}) do

    # Prevent access from other student's page.
    if conn.assigns.current_user.username == username do

      # TODO: If quiz for the day has already been submitted,
      #       disable 'take quiz' button.
      conn = assign(conn, :take_quiz_button_class, "")
      
      query = from h in History, 
                select: h.inserted_at,
                where: h.user_id == ^conn.assigns.current_user.id

      date_of_latest_entry = Repo.all(query) 

      unless Enum.empty? date_of_latest_entry do
        date_of_latest_entry = date_of_latest_entry |> List.last |> Timex.to_date
        today = Timex.today

        cond do
          Timex.equal?(today, date_of_latest_entry) -> 
            conn = assign(conn, :take_quiz_button_class, "disabled")
          true ->
            conn 
        end
      end

      query = from student_lesson in StudentLesson,
                join: user in assoc(student_lesson, :user),
                join: lesson in assoc(student_lesson, :lesson),
                preload: [:user, lesson: :subject],
                order_by: [desc: student_lesson.inserted_at],
                where: user.username == ^username

      assigned_lessons = Repo.all query

      render(conn, "show.html", assigned_lessons: assigned_lessons)
    else
      conn
      |> put_flash(:error, "You are not authorized to access that page.")
      |> redirect(to: student_path(conn, :show, username))
    end

  end

  def history(conn, %{"username" => username}) do

    # Prevent access from other student's page.
    if conn.assigns.current_user.username == username do
      query = from h in History, where: h.user_id == ^conn.assigns.current_user.id
      history = Repo.all(query) |> Repo.preload(lesson: :subject)

      render(conn, "history.html", history: history)
    else
      conn
      |> put_flash(:error, "You are not authorized to access that page.")
      |> redirect(to: student_path(conn, :show, username))
    end

  end

  def review(conn, %{"username" => username, "lesson_id" => lesson_id}) do

    # Prevent access from other student's page.
    if conn.assigns.current_user.username == username do
      lesson = Repo.get!(Lesson, lesson_id) |> Repo.preload(:terms)

      render(conn, "review.html", lesson: lesson, term_count: Enum.count(lesson.terms))
    else
      conn
      |> put_flash(:error, "You are not authorized to access that page.")
      |> redirect(to: student_path(conn, :show, username))
    end

  end

  def quiz(conn, %{"username" => username, "lesson_id" => lesson_id}) do

    conn = assign(conn, :quiz_started_at, Timex.now)

    # Prevent access from other student's page.
    if conn.assigns.current_user.username == username do
      lesson =
        Lesson
        |> Repo.get!(lesson_id) 
        |> Repo.preload(terms: from(t in Term, order_by: fragment("RANDOM()"), limit: 10))

      ids = Enum.map lesson.terms, fn %{id: id} -> id end
      ids = Enum.join ids, " "

      render(conn, "quiz.html", lesson: lesson, terms: lesson.terms, ids: ids)
    else
      conn
      |> put_flash(:error, "You are not authorized to access that page.")
      |> redirect(to: student_path(conn, :show, username))
    end

  end

  def submit_quiz(conn, %{"lesson_id" => lesson_id, "answers" => answers, "answer" => answer}) do
    
    lesson =
      Lesson
      |> Repo.get!(lesson_id) 
      |> Repo.preload(:terms)

    ids = String.split(answers["ids"])

    # Check student's answer. Return list of maps containing definition, score,
    # correct term, student's answer and term id.
    results = Enum.map(ids, fn(id) -> check_answer(lesson.terms, answer, id) end)

    # Loop over results to compute total score. Return tuple containing original
    # results and total score.
    results = Enum.map_reduce(results, 0, fn(result, acc) -> {result, result.score + acc} end)
    
    quiz_started_at = answers["quiz_started_at"] |> Timex.parse!("{ISO:Extended:Z}")
    score = elem(results, 1)
    total_items = length(elem(results, 0))

    changeset = History.changeset(%History{user_id: conn.assigns.current_user.id,
                                           lesson_id: lesson.id,
                                           score: score,
                                           total_items: total_items,
                                           quiz_started_at: quiz_started_at})

    case Repo.insert(changeset) do
      {:ok, struct} -> 
        # Save incorrect answers to database.
        Enum.each elem(results, 0), fn result ->
          if result.score == 0, do: Repo.insert %Mistake{history_id: struct.id, term_id: result.term_id}
        end

    end

    render(conn, "quiz_result.html", lesson: lesson, results: elem(results, 0), score: score, total_items: total_items)
    
  end

  defp check_answer(terms, answer, id) do
    stored =
      Enum.filter(terms, fn(term) -> term.id == String.to_integer(id) end)
      |> hd

    submitted =
      Enum.filter(answer, fn(ans) -> elem(ans, 0) == id end)
      |> hd

    stored_term = stored.term
    submitted_term = elem(submitted, 1)

    score = cond do
      String.downcase(stored_term) == String.downcase(submitted_term) -> 1
      true -> 0
    end

    %{term_id: stored.id, definition: stored.definition, stored_term: stored_term, submitted_term: submitted_term, score: score}
  end

  def assign_lesson(conn, %{"student_lesson" => student_lesson_params}) do
    user = Repo.get!(User, student_lesson_params["user_id"])
    student_lesson = Repo.get_by(StudentLesson, user_id: student_lesson_params["user_id"], lesson_id: student_lesson_params["lesson_id"])

    # Check if lesson was already assigned to this student.
    if student_lesson == nil do
      changeset = StudentLesson.changeset(%StudentLesson{}, student_lesson_params)

      case Repo.insert(changeset) do
        {:ok, _student} ->
          conn
          |> put_flash(:info, "Lesson assigned.")
          |> redirect(to: user_path(conn, :show, user))
        {:error, _changeset} ->
          conn 
          |> put_flash(:error, "Error assigning lesson.")
          |> redirect(to: user_path(conn, :show, user))
      end
    else
      conn 
      |> put_flash(:error, "Lesson already assigned.")
      |> redirect(to: user_path(conn, :show, user))
    end

  end

  def unassign_lesson(conn, %{"student_id" => student_id, "lesson_id" => lesson_id}) do
    query = from a in StudentLesson,
            where: a.user_id == ^student_id,
            where: a.lesson_id == ^lesson_id
    student_lesson = Repo.one!(query)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(student_lesson)

    conn
    |> put_flash(:info, "Lesson unassigned successfully.")
    |> redirect(to: user_path(conn, :show, student_lesson.user_id))
  end

end
