defmodule Ioquiz.PageController do
  use Ioquiz.Web, :controller

  def index(conn, _params) do
    conn
    |> put_layout("public.html")
    |> render("index.html")
  end
end
