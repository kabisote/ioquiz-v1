defmodule Ioquiz.ItemController do
  use Ioquiz.Web, :controller

  alias Ioquiz.Subject
  alias Ioquiz.Lesson
  alias Ioquiz.Question
  alias Ioquiz.Item

  def index(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id, "question_id" => question_id}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(Lesson, lesson_id)
    question = Repo.get!(Question, question_id)
    items = Repo.all(question_items(question))

    render(conn, "index.html", subject: subject, lesson: lesson, question: question, items: items)
  end

  def new(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id, "question_id" => question_id}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(Lesson, lesson_id)
    question = Repo.get!(Question, question_id)

    changeset = 
      question
      |> build_assoc(:items)
      |> Item.changeset()

    render(conn, "new.html", subject: subject, lesson: lesson, question: question, changeset: changeset)
  end

  def create(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id, "question_id" => question_id, "item" => item_params}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(Lesson, lesson_id)
    question = Repo.get!(Question, question_id)

    changeset = 
      question
      |> build_assoc(:items)
      |> Item.changeset(item_params)

    case Repo.insert(changeset) do
      {:ok, _item} ->
        conn
        |> put_flash(:info, "Item created successfully.")
        |> redirect(to: subject_lesson_question_item_path(conn, :index, subject, lesson, question))
      {:error, changeset} ->
        render(conn, "new.html", subject: subject, lesson: lesson, question: question, changeset: changeset)
    end
  end

  def edit(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id, "question_id" => question_id, "id" => id}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(Lesson, lesson_id)
    question = Repo.get!(Question, question_id)
    item = Repo.get!(question_items(question), id)

    changeset = Item.changeset(item)

    render(conn, "edit.html", subject: subject, lesson: lesson, question: question, item: item, changeset: changeset)
  end

  def update(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id, "question_id" => question_id, "id" => id, "item" => item_params}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(Lesson, lesson_id)
    question = Repo.get!(Question, question_id)
    item = Repo.get!(question_items(question), id)

    changeset = Item.changeset(item, item_params)

    case Repo.update(changeset) do
      {:ok, _item} ->
        conn
        |> put_flash(:info, "Item updated successfully.")
        |> redirect(to: subject_lesson_question_item_path(conn, :index, subject, lesson, question))
      {:error, changeset} ->
        render(conn, "edit.html", subject: subject, lesson: lesson, question: question, item: item, changeset: changeset)
    end
  end

  def delete(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id, "question_id" => question_id, "id" => id}) do
    item = Repo.get!(Item, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(item)

    conn
    |> put_flash(:info, "Item deleted successfully.")
    |> redirect(to: subject_lesson_question_item_path(conn, :index, subject_id, lesson_id, question_id))
  end

  defp question_items(question) do
    assoc(question, :items)
  end
end
