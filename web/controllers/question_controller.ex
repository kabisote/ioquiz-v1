defmodule Ioquiz.QuestionController do
  use Ioquiz.Web, :controller

  alias Ioquiz.Subject
  alias Ioquiz.Lesson
  alias Ioquiz.Question

  def index(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(Lesson, lesson_id)
    questions = Repo.all(lesson_questions(lesson))

    render(conn, "index.html", subject: subject, lesson: lesson, questions: questions, question_count: Enum.count(questions))
  end

  def new(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(Lesson, lesson_id)

    changeset = 
      lesson
      |> build_assoc(:questions)
      |> Question.changeset()

    render(conn, "new.html", subject: subject, lesson: lesson, changeset: changeset)
  end

  def create(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id, "question" => question_params}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(Lesson, lesson_id)

    changeset = 
      lesson
      |> build_assoc(:questions)
      |> Question.changeset(question_params)

    case Repo.insert(changeset) do
      {:ok, _question} ->
        conn
        |> put_flash(:info, "Question created successfully.")
        |> redirect(to: subject_lesson_question_path(conn, :index, subject, lesson))
      {:error, changeset} ->
        render(conn, "new.html", subject: subject, lesson: lesson, changeset: changeset)
    end
  end

  def edit(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id, "id" => id}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(Lesson, lesson_id)
    question = Repo.get!(lesson_questions(lesson), id)

    changeset = Question.changeset(question)

    render(conn, "edit.html", subject: subject, lesson: lesson, question: question, changeset: changeset)
  end

  def update(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id, "id" => id, "question" => question_params}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(Lesson, lesson_id)
    question = Repo.get!(lesson_questions(lesson), id)

    changeset = Question.changeset(question, question_params)

    case Repo.update(changeset) do
      {:ok, _question} ->
        conn
        |> put_flash(:info, "Question updated successfully.")
        |> redirect(to: subject_lesson_question_path(conn, :index, subject, lesson))
      {:error, changeset} ->
        render(conn, "edit.html", subject: subject, lesson: lesson, question: question, changeset: changeset)
    end
  end

  def delete(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id, "id" => id}) do
    question = Repo.get!(Question, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(question)

    conn
    |> put_flash(:info, "Question deleted successfully.")
    |> redirect(to: subject_lesson_question_path(conn, :index, subject_id, lesson_id))
  end

  defp lesson_questions(lesson) do
    assoc(lesson, :questions)
  end
end
