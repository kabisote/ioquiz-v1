defmodule Ioquiz.TermController do
  use Ioquiz.Web, :controller

  alias Ioquiz.Subject
  alias Ioquiz.Term

  def index(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(subject_lessons(subject), lesson_id)
    terms = Repo.all(lesson_terms(lesson))

    render(conn, "index.html", subject: subject, lesson: lesson, terms: terms, term_count: Enum.count(terms))
  end

  def new(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(subject_lessons(subject), lesson_id)

    changeset = 
      lesson
      |> build_assoc(:terms)
      |> Term.changeset()

    render(conn, "new.html", subject: subject, lesson: lesson, changeset: changeset)
  end

  def create(conn, %{"term" => term_params, "subject_id" => subject_id, "lesson_id" => lesson_id}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(subject_lessons(subject), lesson_id)

    changeset = 
      lesson
      |> build_assoc(:terms)
      |> Term.changeset(term_params)

    case Repo.insert(changeset) do
      {:ok, _term} ->
        conn
        |> put_flash(:info, "Term created successfully.")
        |> redirect(to: subject_lesson_term_path(conn, :index, subject, lesson))
      {:error, changeset} ->
        render(conn, "new.html", subject: subject, lesson: lesson, changeset: changeset)
    end
  end

  def edit(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id, "id" => id}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(subject_lessons(subject), lesson_id)
    term = Repo.get!(lesson_terms(lesson), id)

    changeset = Term.changeset(term)

    render(conn, "edit.html", subject: subject, lesson: lesson, term: term, changeset: changeset)
  end

  def update(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id, "id" => id, "term" => term_params}) do
    subject = Repo.get!(Subject, subject_id)
    lesson = Repo.get!(subject_lessons(subject), lesson_id)
    term = Repo.get!(lesson_terms(lesson), id)

    changeset = Term.changeset(term, term_params)

    case Repo.update(changeset) do
      {:ok, _term} ->
        conn
        |> put_flash(:info, "Term updated successfully.")
        |> redirect(to: subject_lesson_term_path(conn, :index, subject, lesson))
      {:error, changeset} ->
        render(conn, "edit.html", subject: subject, lesson: lesson, term: term, changeset: changeset)
    end
  end

  def delete(conn, %{"subject_id" => subject_id, "lesson_id" => lesson_id, "id" => id}) do
    term = Repo.get!(Term, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(term)

    conn
    |> put_flash(:info, "Term deleted successfully.")
    |> redirect(to: subject_lesson_term_path(conn, :index, subject_id, lesson_id))
  end

  defp subject_lessons(subject) do
    assoc(subject, :lessons)
  end

  defp lesson_terms(lesson) do
    assoc(lesson, :terms)
  end
end
