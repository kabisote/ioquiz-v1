// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"

$(document).ready(function() {

  // progress bar on review page
  var $progressor = document.getElementById('review_progress'),
      $prevButton = $('.ui.button.prev'),
      $nextButton = $('.ui.button.next');

  $('.slick-test').slick({
    adaptiveHeight: true,
    arrows: false,
    infinite: false,
  });

  $('#review_progress').progress({
    text: {
      active: '{value} of {total} Items',
      success: '{value} of {total} Items'
    }
  });

  $prevButton.on('click', function(){
    $('.slick-test').slick('slickPrev').on('afterChange', function(event, slick, currentSlide){
      var currentSlideNumber = currentSlide + 1;

      if (currentSlideNumber <= 1) {
        $prevButton.addClass('disabled');
      }

      if (currentSlideNumber < $progressor.dataset.total && $nextButton.hasClass('disabled')) {
        $nextButton.removeClass('disabled');
      }
    });

    $('#review_progress').progress('increment', -1);
  });

  $nextButton.on('click', function(){
    $('.slick-test').slick('slickNext').on('afterChange', function(event, slick, currentSlide){
      var currentSlideNumber = currentSlide + 1;

      if (currentSlideNumber >= $progressor.dataset.total) {
        $nextButton.addClass('disabled');
      }

      if (currentSlideNumber > 1 && $prevButton.hasClass('disabled')) {
        $prevButton.removeClass('disabled');
      }
    });

    $('#review_progress').progress('increment');
  });

  // flash cards on review page
  $('#flash-cards .ui.button').on('click', function(){
    $(this).next('.ui.shape').shape('flip back');
  });

  $('textarea#term_term').trumbowyg();
  $('textarea#term_definition').trumbowyg();

})
;
