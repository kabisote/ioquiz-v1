defmodule Ioquiz.Item do
  use Ioquiz.Web, :model

  schema "items" do
    field :content, :string
    field :type, :integer, default: 1
    field :status, :integer, default: 1
    belongs_to :question, Ioquiz.Question

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:content, :type, :status])
    |> validate_required([:content])
  end
end
