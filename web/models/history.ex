defmodule Ioquiz.History do
  use Ioquiz.Web, :model

  schema "history" do
    field :score, :integer
    field :total_items, :integer
    field :quiz_started_at, Timex.Ecto.DateTime
    belongs_to :user, Ioquiz.User
    belongs_to :lesson, Ioquiz.Lesson
    has_many :mistakes, Ioquiz.Mistake

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:user_id, :lesson_id])
    |> assoc_constraint(:user)
    |> assoc_constraint(:lesson)
  end
end
