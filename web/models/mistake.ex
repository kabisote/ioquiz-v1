defmodule Ioquiz.Mistake do
  use Ioquiz.Web, :model

  schema "mistakes" do
    belongs_to :history, Ioquiz.History
    belongs_to :term, Ioquiz.Term

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:history_id, :term_id])
    |> assoc_constraint(:history)
    |> assoc_constraint(:term)
  end
end

