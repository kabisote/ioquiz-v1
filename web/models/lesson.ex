defmodule Ioquiz.Lesson do
  use Ioquiz.Web, :model

  schema "lessons" do
    field :title, :string
    belongs_to :subject, Ioquiz.Subject
    has_many :questions, Ioquiz.Question
    has_many :terms, Ioquiz.Term
    many_to_many :users, Ioquiz.User, join_through: "student_lessons"

    timestamps()
  end
  
  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title])
    |> validate_required([:title])
    |> assoc_constraint(:subject)
  end
end
