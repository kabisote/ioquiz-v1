defmodule Ioquiz.Subject do
  use Ioquiz.Web, :model

  schema "subjects" do
    field :title, :string
    has_many :lessons, Ioquiz.Lesson

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title])
    |> validate_required([:title])
    |> unique_constraint(:title)
  end
end
