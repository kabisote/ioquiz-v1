defmodule Ioquiz.Question do
  use Ioquiz.Web, :model

  schema "questions" do
    field :question, :string
    field :type, :integer, default: 1 # 1 = identification, 2 = multiple choice, etc..
    field :status, :integer, default: 1 # 1 = active, 2 = inactive, 3 = draft
    belongs_to :lesson, Ioquiz.Lesson
    has_many :items, Ioquiz.Item

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:question, :type, :status])
    |> validate_required([:question])
  end
end
