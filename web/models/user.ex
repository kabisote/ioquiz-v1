defmodule Ioquiz.User do
  use Ioquiz.Web, :model

  schema "users" do
    field :name, :string
    field :username, :string
    field :role_level, :integer, default: 3 # 1 = administrator, 2 = teacher, 3 = student
    field :password, :string, virtual: true
    field :password_hash, :string
    field :details, :string
    many_to_many :lessons, Ioquiz.Lesson, join_through: "student_lessons"

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :username, :role_level, :details])
    |> validate_required([:name, :username, :role_level])
    |> unique_constraint(:username)
  end

  def registration_changeset(struct, params) do
    struct
    |> changeset(params)
    |> cast(params, [:password])
    |> validate_required([:password])
    |> validate_length(:password, min: 6, max: 100)
    |> put_pass_hash()
  end

  defp put_pass_hash(struct) do
    case struct do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(struct, :password_hash, Comeonin.Bcrypt.hashpwsalt(pass))
      _ ->
        struct
    end
  end
end
