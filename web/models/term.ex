defmodule Ioquiz.Term do
  use Ioquiz.Web, :model

  schema "terms" do
    field :term, :string
    field :definition, :string
    belongs_to :lesson, Ioquiz.Lesson

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:term, :definition])
    |> validate_required([:term, :definition])
  end
end
