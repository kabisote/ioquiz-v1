defmodule Ioquiz.StudentLesson do
  use Ioquiz.Web, :model

  schema "student_lessons" do
    belongs_to :user, Ioquiz.User
    belongs_to :lesson, Ioquiz.Lesson

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:user_id, :lesson_id])
    |> validate_required([:user_id, :lesson_id])
  end
end
