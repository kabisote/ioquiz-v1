defmodule Ioquiz.StudentLessonTest do
  use Ioquiz.ModelCase

  alias Ioquiz.StudentLesson

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = StudentLesson.changeset(%StudentLesson{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = StudentLesson.changeset(%StudentLesson{}, @invalid_attrs)
    refute changeset.valid?
  end
end
