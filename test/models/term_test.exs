defmodule Ioquiz.TermTest do
  use Ioquiz.ModelCase

  alias Ioquiz.Term

  @valid_attrs %{definition: "some content", term: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Term.changeset(%Term{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Term.changeset(%Term{}, @invalid_attrs)
    refute changeset.valid?
  end
end
