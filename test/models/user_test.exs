defmodule Ioquiz.UserTest do
  use Ioquiz.ModelCase

  alias Ioquiz.User

  @valid_attrs %{details: "some content", name: "some content", password_hash: "some content", role: "some content", username: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end
end
